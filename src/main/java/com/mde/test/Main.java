package com.mde.test; /**
 * Created by Dmitry Mishchenko on 10.09.2015.
 */
import java.io.*;

public class Main {

    public static void main(String args[]) throws FileNotFoundException {
        String s = null;

        PrintWriter file = new PrintWriter(new File("log.txt"));

        try {
            Process p = Runtime.getRuntime().exec("phantomjs webpage-screen.js");

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            // read the output from the command
            file.println("Here is the standard output of the command:\n");
            while ((s = stdInput.readLine()) != null) {
                file.println(s);
            }

            // read any errors from the attempted command
            file.println("Here is the standard error of the command (if any):\n");
            while ((s = stdError.readLine()) != null) {
                file.println(s);
            }

        }
        catch (IOException e) {
            file.println("exception happened - here's what I know: ");
            e.printStackTrace();
            System.exit(-1);
        }
        file.close();

        System.exit(0);
    }
}